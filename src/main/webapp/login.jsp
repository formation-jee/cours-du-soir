<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<html>
<body>
<h2>Veuillez vous connecter !</h2>

<!--Ceci est un commentaire html ! -->

<%-- Ceci est un commentaire sur notre JSP --%>

<%
    Date date = new Date();

    SimpleDateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
    String dateString = formater.format(date);

    out.println("Nous sommes le " + dateString);
%>

<form method="post" action="login">
    <label>Username</label>
    <input type="text" name="username" <%
        if(request.getMethod().equals("POST")){
            out.println("value=\""+request.getAttribute("lastUsername")+"\" ");
        }
    %> placeholder="Veuillez saisir un username"/>

    <label>Password</label>
    <input type="password" name="password" placeholder="Veuillez saisir un mot de passe">

    <input type="submit">

    <c:if test="${pageContext.request.method == 'POST'}">
        <c:forEach items="${requestScope.get('errors')}" var="error">
            <li><c:out value="${error}"/></li>
        </c:forEach>
    </c:if>

</form>
</body>
</html>
