package com.humanbooster.coursdusoir.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DashboardServlet extends HttpServlet {
    // Mon utilisateur se retrouve donc dans la mèthode doget du dashboard servlet
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        req.getRequestDispatcher("dashboard.jsp").forward(req, res);
    }
}
