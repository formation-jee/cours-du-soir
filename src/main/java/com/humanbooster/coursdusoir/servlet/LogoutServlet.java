package com.humanbooster.coursdusoir.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogoutServlet extends HttpServlet {

    // Mon utilisateur se retrouve donc dans la mèthode doget du dashboard servlet
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        req.getSession(false).removeAttribute("username");
        req.getSession().invalidate();
        // TODO verify why session is valid !
        res.sendRedirect("login.jsp");
        return;
    }
}
