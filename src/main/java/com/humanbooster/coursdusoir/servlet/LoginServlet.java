package com.humanbooster.coursdusoir.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;


public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {

        // Je réccupére la valeur de l'attribut password dans le corps de ma requête
        String password = req.getParameter("password");
        // Je réccupére la valeur de l'attribut username dans le corps de ma requête
        String username = req.getParameter("username");

        // Je réclare une liste d'erreur vide
        ArrayList<String> errors = new ArrayList<String>();

        // Je regarde si le couple utilisateur / password est admin / admin
        if(username.equals("admin") && password.equals("admin")){
            // J'ajoute mon nom d'utilisateur en session
            req.getSession().setAttribute("username", username);
            // Je redirige mon utilisateur vers l'url dashboard
            res.sendRedirect("dashboard");

        } else {

            if(!password.equals("admin")){
                errors.add("Le mot de passe est incorrecte");
            }

            if(!username.equals("admin")){
                errors.add("Le username est incorrecte !");
            }

            req.setAttribute("lastUsername", username);
            req.setAttribute("errors", errors);
            req.getRequestDispatcher("login.jsp").forward(req, res);
            // Renvoyer notre page de login avec les erreurs
        }
    }

}
