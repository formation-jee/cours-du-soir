package com.humanbooster.coursdusoir.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class DisplaySessionServlet extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        PrintWriter out = resp.getWriter();

        String username = (String) req.getSession().getAttribute("username");

        out.println("<html>" +
                "<head></head>" +
                "<body>" +
                "<h1>Welcome to session servlet !</h1>" +
                "<h2>En session tu t'appelles "+username+"</h1>" +
                "</body>" +
                "</html>");
    }
}
